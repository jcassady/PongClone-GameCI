using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace JordanCassady
{
    public class GameManagerTests
    {
        private Ball ball;
        private ComputerPaddle computerPaddle;
        private GameManager gameManager;

        [UnitySetUp]
        public IEnumerator SetupBall()
        {
            GameObject ballGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Ball") as GameObject);
            ball = ballGameObject.GetComponent<Ball>();
            yield return null;
        }

        [UnitySetUp]
        public IEnumerator SetupComputerPaddle()
        {
            GameObject computerPaddleGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Computer Paddle") as GameObject);
            GameObject.DestroyImmediate(computerPaddleGameObject.GetComponent<Collider2D>());
            computerPaddle = computerPaddleGameObject.GetComponent<ComputerPaddle>();
            computerPaddle.TrackBall(ball);
            yield return null;
        }

        [UnitySetUp]
        public IEnumerator SetupGameManager()
        {
            GameObject GameManagerGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Game Manager") as GameObject);
            gameManager = GameManagerGameObject.GetComponent<GameManager>();
            gameManager.TrackBall(ball);
            yield return null;
        }

        [UnityTest]
        public IEnumerator ComputerScores_ScoreZoneCollision_ScoreIncrements()
        {
            gameManager.ComputerScores(); // Origin
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(gameManager.ComputerScore == 1);
        }

        [UnityTest]
        public IEnumerator PlayerScores_ScoreZoneCollision_ScoreIncrements()
        {
            gameManager.PlayerScores();
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(gameManager.PlayerScore == 1);
        }
    }
}