using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace JordanCassady
{
    public class BallTests
    {
        private Ball ball;

        [UnitySetUp]
        public IEnumerator SetupBallGameObject()
        {
            GameObject ballGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Ball") as GameObject);
            ball = ballGameObject.GetComponent<Ball>();
            GameObject.DestroyImmediate(ball.GetComponent<BoxCollider2D>());
            yield return null;
        }

        [UnityTest]
        public IEnumerator Tipoff_BallGameObject_MovesOnX()
        {
            ball.Tipoff(BallDirection.Random);
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(ball.transform.position.x < 0f ||
                            ball.transform.position.x > 0f);
        }

        [UnityTest]
        public IEnumerator Tipoff_BallGameObject_MovesOnY()
        {
            ball.Tipoff(BallDirection.Random);
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(ball.transform.position.y < 0f ||
                            ball.transform.position.y > 0f);
        }

        [UnityTest]
        public IEnumerator MoveDirection_BallGameObject_MovesInDirection()
        {
            ball.MoveDirection(new Vector2(1f, 0f));
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(ball.transform.position.x, 0.001f);
        }

        [UnityTest]
        public IEnumerator ResetPosition_BallGameObject_ScreenCenterPosition()
        {
            ball.ResetPosition();
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(ball.GetComponent<Rigidbody2D>().position.x == 0f);
            Assert.IsTrue(ball.GetComponent<Rigidbody2D>().position.y == 0f);
        }
    }
}