using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace JordanCassady
{
    [TestFixture]
    public class SurfaceBounceTests
    {
        private Ball ball;

        [UnitySetUp]
        public IEnumerator SetupBall()
        {
            GameObject ballGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Ball") as GameObject);
            ball = ballGameObject.GetComponent<Ball>();
            yield return null;
        }

        [UnitySetUp]
        public IEnumerator SetupWalls()
        {
            GameObject.Instantiate(Resources.Load("Prefabs/Top Wall") as GameObject);
            GameObject.Instantiate(Resources.Load("Prefabs/Bottom Wall") as GameObject);
            yield return null;
        }

        [UnityTest]
        public IEnumerator SurfaceBounce_WallCollision_BallVelocityIncreases()
        {
            ball.speed = 1000;
            ball.Tipoff(BallDirection.Up);
            yield return new WaitForSeconds(0.3f);
            float absVelocityY = Mathf.Abs(ball.GetComponent<Rigidbody2D>().velocity.y);
            Assert.GreaterOrEqual(absVelocityY, 20f);
        }
    }
}