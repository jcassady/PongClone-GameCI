using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

// Classes PaddleMovesDown and PaddleMovesUp to workaround ball tracking issue
// when using single class.

namespace JordanCassady
{
    [TestFixture]
    public class ComputerPaddleTests
    {
        private Ball ball;
        private ComputerPaddle computerPaddle;
        private GameObject computerPaddleGameObject;

        [UnitySetUp]
        public IEnumerator SetupBall()
        {
            GameObject ballGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Ball") as GameObject);
            ball = ballGameObject.GetComponent<Ball>();
            yield return null;
        }

        [UnitySetUp]
        public IEnumerator SetupComputerPaddle()
        {
            computerPaddleGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Computer Paddle") as GameObject);
            GameObject.DestroyImmediate(computerPaddleGameObject.GetComponent<Collider2D>());
            computerPaddle = computerPaddleGameObject.GetComponent<ComputerPaddle>();
            computerPaddle.TrackBall(ball);
            yield return null;
        }

        // Only test one ball direction. Need way to assign uniquely tracked balls.
        [UnityTest]
        public IEnumerator MovePaddle_DownwardBallProjection_PaddleMovesDown()
        {
            // Project ball below the computer paddle.
            ball.Tipoff(BallDirection.RightDown);

            // Ensure computer paddle moves down towards ball projection.
            yield return new WaitForSeconds(0.3f);

            Assert.LessOrEqual(computerPaddle.transform.position.y, -0.1f);
        }

        // Only test one ball direction. Need way to assign uniquely tracked balls.
        [UnityTest]
        public IEnumerator MovePaddle_UpwardBallProjection_PaddleMovesUp()
        {
            // Project ball below the computer paddle.
            ball.Tipoff(BallDirection.RightUp);

            // Ensure computer paddle moves down towards ball projection.
            yield return new WaitForSeconds(0.3f);

            Assert.GreaterOrEqual(computerPaddle.transform.position.y, 0.1f);
        }

        /// <summary>
        /// Project ball quickly to the right for quick testing.
        /// </summary>
        [UnityTest]
        public IEnumerator MovePaddle_BallCollision_PaddleReturnsBall()
        {
            computerPaddleGameObject.AddComponent<BoxCollider2D>();
            ball.speed = 2000;
            ball.Tipoff(BallDirection.Right);

            yield return new WaitForSeconds(0.3f);

            var ballVelocity = ball.GetComponent<Rigidbody2D>()
                .GetPointVelocity(computerPaddle.transform.position);

            Assert.LessOrEqual(ballVelocity.x, -0.1f);
        }

        [UnityTest]
        public IEnumerator ResetPosition_ComputerPaddle_ScreenRightPosition()
        {
            computerPaddle.ResetPosition();
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(computerPaddle.GetComponent<Rigidbody2D>().position.x == 8);
            Assert.IsTrue(computerPaddle.GetComponent<Rigidbody2D>().position.y == 0);
            Assert.IsTrue(computerPaddle.GetComponent<Rigidbody2D>().velocity == Vector2.zero);
        }
    }
}



