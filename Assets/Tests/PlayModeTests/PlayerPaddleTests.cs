using NUnit.Framework;
using NSubstitute;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace JordanCassady
{
    [TestFixture]
    public class PlayerPaddleTests
    {
        private PlayerPaddle playerPaddle;

        [UnitySetUp]
        public IEnumerator SetupPlayerPaddle()
        {
            GameObject playerPaddleGameObject = GameObject.Instantiate(
                Resources.Load("Prefabs/Player Paddle") as GameObject);
            GameObject.DestroyImmediate(playerPaddleGameObject.GetComponent<Collider2D>());
            playerPaddle = playerPaddleGameObject.GetComponent<PlayerPaddle>();
            playerPaddle.PlayerInput = Substitute.For<IPlayerInput>();
            yield return null;
        }

        [UnityTest]
        public IEnumerator VerticalDirection_InputVectorIsNull_PaddleDoesNotMove()
        {
            // Do not move the player paddle.
            playerPaddle.PlayerInput.VerticalDirection.Returns(Vector2.zero);
            yield return new WaitForSeconds(0.1f);

            Assert.IsTrue(playerPaddle.transform.position.y == 0f);
            Assert.AreEqual(-8f, playerPaddle.transform.position.x);
            Assert.AreEqual(0, playerPaddle.transform.position.z);
        }

        [UnityTest]
        public IEnumerator VerticalDirection_InputVectorIsDown_PaddleMovesDown()
        {
            // Move the player paddle downwards in the vertical direction.
            playerPaddle.PlayerInput.VerticalDirection.Returns(Vector2.down);
            yield return new WaitForSeconds(0.1f);

            // Ensure player paddle is only moving downwards on Y axis.
            Assert.IsTrue(playerPaddle.transform.position.y < 0f);
            Assert.AreEqual(-8f, playerPaddle.transform.position.x);
            Assert.AreEqual(0, playerPaddle.transform.position.z);
        }

        [UnityTest]
        public IEnumerator VerticalDirection_InputVectorIsUp_PaddleMovesUp()
        {
            // Move the player paddle upwards in the vertical direction.
            playerPaddle.PlayerInput.VerticalDirection.Returns(Vector2.up);
            yield return new WaitForSeconds(0.1f);

            // Ensure player paddle is only moving upwards on Y axis.
            Assert.IsTrue(playerPaddle.transform.position.y > 0f);
            Assert.AreEqual(-8f, playerPaddle.transform.position.x);
            Assert.AreEqual(0, playerPaddle.transform.position.z);
        }

        [UnityTest]
        public IEnumerator ResetPosition_PlayerPaddle_ScreenLeftPosition()
        {
            playerPaddle.ResetPosition();
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(playerPaddle.GetComponent<Rigidbody2D>().position.x == -8);
            Assert.IsTrue(playerPaddle.GetComponent<Rigidbody2D>().position.y == 0);
            Assert.IsTrue(playerPaddle.GetComponent<Rigidbody2D>().velocity == Vector2.zero);
        }
    }
}