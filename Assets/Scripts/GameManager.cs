using UnityEngine;
using UnityEngine.UI;

namespace JordanCassady
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Ball ball;

        public int ComputerScore => _computerScore;
        public int PlayerScore => _playerScore;

        public Text _computerScoreText;
        public Text _playerScoreText;
        public ComputerPaddle _computerPaddle;
        public PlayerPaddle _playerPaddle;

        private int _playerScore;
        private int _computerScore;

        private void Awake()
        {
            // Null check used with TrackBall in test.
            if (ball != null)
            {
                ball.ResetPosition();
                ball.Tipoff(BallDirection.Random);
            }
        }

        private void ResetRound()
        {
            _computerPaddle.ResetPosition();
            _playerPaddle.ResetPosition();
            ball.ResetPosition();
            ball.Tipoff(BallDirection.Random);
        }

        public void ComputerScores()
        {
            _computerScore++;
            _computerScoreText.text = _computerScore.ToString();
            ResetRound();
        }

        public void PlayerScores()
        {
            _playerScore++;
            _playerScoreText.text = _playerScore.ToString();
            ResetRound();
        }

        public void TrackBall(Ball trackBall)
        {
            ball = trackBall;
        }
    }
}

