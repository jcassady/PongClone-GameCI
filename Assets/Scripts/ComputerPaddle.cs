using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JordanCassady
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ComputerPaddle : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D ballRigidbody;

        private readonly float _speed = 10.0f;

        private void FixedUpdate()
        {
            MovePaddle();
        }

        /// <summary>
        /// Assign a ball for the computer paddle to track. Useful when there
        /// are multiple balls in the scene, like when running all unit tests.
        /// </summary>
        /// <param name="ballRigidbody"></param>
        public void TrackBall(Ball ball)
        {
            ballRigidbody = ball.GetComponent<Rigidbody2D>();
        }

        /// <summary>
        /// If ball projection is above paddle, move up. If ball projection
        /// is below paddle, move down.
        /// </summary>
        private void MovePaddle()
        {
            // When ball moving towards computer oponent, move the paddle
            // towards the ball trajectory.
            if (ballRigidbody.velocity.x > 0f)
            {
                // If ball is above the oponent, move the paddle up.
                if (ballRigidbody.transform.position.y > transform.position.y)
                    GetComponent<Rigidbody2D>().AddForce(Vector2.up * _speed);
                // If ball is below the oponent, move the paddle down.
                else if (ballRigidbody.transform.position.y < transform.position.y)
                    GetComponent<Rigidbody2D>().AddForce(Vector2.down * _speed);
            }
            else // Idle paddle screen center for better defence.
            {
                if (transform.position.y > 0f)
                    GetComponent<Rigidbody2D>().AddForce(Vector2.down * _speed);
                else if (transform.position.y < 0f)
                    GetComponent<Rigidbody2D>().AddForce(Vector2.up * _speed);
            }
        }

        public void ResetPosition()
        {
            GetComponent<Rigidbody2D>().position = new Vector2(GetComponent<Rigidbody2D>().transform.position.x, 0f);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
