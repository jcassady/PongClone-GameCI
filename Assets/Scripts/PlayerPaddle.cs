using UnityEngine;

namespace JordanCassady
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerPaddle : MonoBehaviour
    {
        public IPlayerInput PlayerInput { get; set; }

        private readonly float _speed = 10.0f;

        private void Awake()
        {
            PlayerInput = GetComponent<PlayerInput>();
        }

        private void FixedUpdate()
        {
            if (PlayerInput.VerticalDirection.sqrMagnitude != 0)
            {
                GetComponent<Rigidbody2D>().AddForce(PlayerInput.VerticalDirection * _speed);
            }
        }

        public void ResetPosition()
        {
            GetComponent<Rigidbody2D>().position = new Vector2(GetComponent<Rigidbody2D>().transform.position.x, 0f);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
