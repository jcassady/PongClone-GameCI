using UnityEngine;

namespace JordanCassady
{
    public class SurfaceBounce : MonoBehaviour
    {
        public float bounceStrength;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();

            if (ball != null)
            {
                // Bounce ball perpendicular at 90 degrees off surface.
                Vector2 normal = collision.GetContact(0).normal;
                ball.MoveDirection(-normal * bounceStrength);
            }
        }
    }
}
