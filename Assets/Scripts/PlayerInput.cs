using UnityEngine;

namespace JordanCassady
{
    public class PlayerInput : MonoBehaviour, IPlayerInput
    {
        public Vector2 VerticalDirection => _verticalDirection;

        private Vector2 _verticalDirection;

        private void Update()
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                _verticalDirection = Vector2.up;
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                _verticalDirection = Vector2.down;
            }
            else
            {
                _verticalDirection = Vector3.zero;
            }
        }
    }
}
