using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JordanCassady
{
    public class ScoreZone : MonoBehaviour
    {
        public GameManager gameManager;
        public EventTrigger.TriggerEvent scoreTrigger;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();

            if (ball != null)
            {
                BaseEventData eventData = new BaseEventData(EventSystem.current);
                scoreTrigger.Invoke(eventData);
            }
        }
    }
}

