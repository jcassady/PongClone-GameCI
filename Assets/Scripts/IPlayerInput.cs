using UnityEngine;

namespace JordanCassady
{
    public interface IPlayerInput
    {
        Vector2 VerticalDirection { get; }
    }
}
