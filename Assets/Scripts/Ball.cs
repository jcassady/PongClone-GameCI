using UnityEngine;

namespace JordanCassady
{
    public enum BallDirection
    {
        Random, Up, Right, RightDown, RightUp
    }

    [RequireComponent(typeof(Rigidbody2D))]
    public class Ball : MonoBehaviour
    {
        public float speed = 200.0f;

        public void Tipoff(BallDirection ballDirection)
        {
            Vector2 paddleDirection = Vector2.zero;
            float randomX = Random.value < 0.5f ? -1f : 1f;
            float randomY = Random.value < 0.5f ? Random.Range(-1f, -0.5f) :
                                                  Random.Range(0.5f, 1f);

            if (ballDirection == BallDirection.Random)
                paddleDirection = new Vector2(randomX, randomY);
            if (ballDirection == BallDirection.Up)
                paddleDirection = new Vector2(0f, 1f);
            else if (ballDirection == BallDirection.Right)
                paddleDirection = new Vector2(1f, 0f);
            else if (ballDirection == BallDirection.RightUp)
                paddleDirection = new Vector2(1f, 1f);
            else if (ballDirection == BallDirection.RightDown)
                paddleDirection = new Vector2(1f, -1f);

            GetComponent<Rigidbody2D>().AddForce(paddleDirection * speed);
        }

        public void MoveDirection(Vector2 moveDirection)
        {
            GetComponent<Rigidbody2D>().AddForce(moveDirection);
        }

        public void ResetPosition()
        {
            GetComponent<Rigidbody2D>().position = Vector2.zero;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
