<h1 align="center">Pong Clone - GameCI</h1>

<h2 align="center"><a  href="https://jcassady.itch.io/pongclone-gameci">Live Demo</a> :joystick:</h2>

<p align="center">
<img src="./readme_assets/h2.png" width="50%">
</p>

## Description :speech_balloon:
**Pong Clone** is a remake of the classic table tennis game, but with a focus on automated testing. Inspired by [Zigurous](https://github.com/zigurous)' classic game remakes, this version has been adapted for the [Unity Test Framework](https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/index.html). Test cases developed in [C#](https://docs.microsoft.com/en-us/dotnet/csharp/).

For an in-depth look at the simulation of player input during automated testing, check out my Medium article:
[Automate Player Input with Unity Test Runner and NSubstitute](https://jordancassady.medium.com/automate-player-input-with-unity-test-runner-and-nsubstitute-70123d17a8f0) :robot:

<p align="center">
<img align="center" src="./readme_assets/pong_clone_pre_start.png" width="40%">
</p>

## Getting started :traffic_light:
This project requires [Unity](http://unity.com/) (2019.4.0+). Clone this repository and open the root folder in Unity.

## Contributing :mega:
Do you have ideas on how you can improve this project? Found a bug, or want to modify the game's behaviour? Please open a [GitLab Issue](https://gitlab.com/jcassady/PongClone-GameCI/-/issues).

## Testing overview :white_check_mark:
This project incorporates unit, integration and end-to-end testing.
<p>
<img src="./readme_assets/playmode_testing.gif" width="80%">
<p>

### Unit tests :black_square_button:
<img src="./readme_assets/unit_testing.png" width="10%">
<p>
<b>Unit tests</b> execute in Unity's PlayMode test runner, and evaluate movement behaviors the Ball, PlayerPaddle, and ComputerPaddle classes in isolation.
<p>

### Integration tests :twisted_rightwards_arrows:
<img src="./readme_assets/integration_testing.png" width="10%">
<p>
<b>Integration tests</b> also execute in Unity's PlayMode test runner, and evaluate collaborating classes such as Ball, PlayerPaddle, and ComputerPaddle.
<p>

### End-to-end tests :arrows_counterclockwise:
<img src="./readme_assets/system_testing.png" width="10%">
<p>
<b>End-to-end tests</b> are performed by playtest volunteers when new builds are deployed to Itch.io.
<p>

## Libraries and Resources Used :books:
1. [Unity Pong Tutorial (GitHub)](https://github.com/zigurous/unity-pong-tutorial)
2. [GameCI (GitLab)](https://gitlab.com/gableroux/unity3d-gitlab-ci-example)
3. [GitLab CI pipelines (Docs)](https://docs.gitlab.com/ee/ci/pipelines/)
4. [NUnit (Docs)](https://docs.nunit.org/)
5. [NSubstitute (GitHub)](https://github.com/Thundernerd/Unity3D-NSubstitute)
6. [Auto Letterbox (Unity Asset Store)](https://assetstore.unity.com/packages/tools/camera/auto-letterbox-56814)

Inspired by [Zigurous](https://github.com/zigurous)'s [Unity Pong Tutorial](https://github.com/zigurous/unity-pong-tutorial), this version has been adapted for Unity PlayMode tests.

## How to play :video_game:
Use the :arrow_up_small:/:arrow_down_small: arrow keys to move the player paddle on the left side of the screen.

## Support the developer :money_with_wings:
Jordan's portfolio: [jordan.cassady.me](https://jordan.cassady.me)
<br>
Camera Composition - Grid Overlay Tool: [Unity Asset Store](https://assetstore.unity.com/packages/tools/camera/camera-composition-grid-overlay-tool-202816)
